const mongoose = require('mongoose')

const lessonSchema = mongoose.Schema(
	{
		name: {
			type: String,
			required: true,
		},
		description: {
			type: String,
			required: false,
		},
		link: {
			type: String,
			required: true,
		},
	},
	{
		timestamps: true,
	}
)

const productSchema = mongoose.Schema(
	{
		user: {
			type: mongoose.Schema.Types.ObjectId,
			required: true,
			ref: 'User',
		},
		name: {
			type: String,
			required: true,
		},
		image: {
			type: String,
			required: true,
		},
		description: {
			type: String,
			required: true,
		},
		rating: {
			type: Number,
			required: true,
			default: 0,
		},
		numReviews: {
			type: Number,
			required: true,
			default: 0,
		},
		lessons: [lessonSchema],
		public: {
			type: Boolean,
			required: true,
			default: true,
		},
		authorName: {
			type: String,
			required: true,
		},
		authorImage: {
			type: String,
			required: true,
		},
		pdf: {
			type: String,
			required: false,
		},
	},
	{
		timestamps: true,
	}
)

module.exports = mongoose.model('Product', productSchema)
