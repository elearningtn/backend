const express = require('express')
const dotenv = require('dotenv')
var cors = require('cors')
const path = require('path')
const { readdirSync } = require('fs')
const connectDatabase = require('./config/database')

dotenv.config({ path: 'config/.env' })

connectDatabase()

const app = express()

app.use(express.json())
app.use(cors())
app.use(express.urlencoded({ extended: false }))
app.use(express.static(path.join(__dirname, 'public')))

readdirSync('routes').map((route) => {
	app.use('/api', require(`./routes/${route}`))
})

const port = process.env.PORT || 4200
app.listen(port, () => {
	console.log(`Server started on port ${port} in ${process.env.NODE_ENV} mode.`)
})
