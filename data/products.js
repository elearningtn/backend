const products = [
	{
		_id: '1',
		name: 'Docker complete course',
		image: '/images/docker.jpg',
		description:
			'Docker is a set of platform as a service products that use OS-level virtualization to deliver software in packages called containers. The service has both free and premium tiers.',
		rating: 4.5,
		numReviews: 12,
		authorId: 1,
		authorName: 'Tarek GuezGuez',
		authorImage: '/images/author.jpg',
		lessons: [
			{
				name: 'Lesson 1: Start Backend',
				description: 'Create new node js express project',
				link: 'https://www.youtube.com/embed/zb3Qk8SG5Ms',
			},
			{
				name: 'Lesson 2: Start Frontend',
				description: 'Create new react js project',
				link: 'https://www.youtube.com/embed/OIBIXYLJjsI',
			},
			{
				name: 'Lesson 3: Deployment',
				description: 'Deploy Mern Project on VPS',
				link: 'https://www.youtube.com/embed/-HPZ1leCV8k',
			},
		],
		public: true,
	},
	{
		_id: '2',
		name: 'Html Css complete course',
		image: '/images/html.jpg',
		description:
			'The HyperText Markup Language or HTML is the standard markup language for documents designed to be displayed in a web browser. It can be assisted by technologies such as Cascading Style Sheets and scripting languages such as JavaScript.',
		rating: 4.0,
		numReviews: 8,
		authorId: 1,
		authorName: 'Tarek GuezGuez',
		authorImage: '/images/author.jpg',
		lessons: [
			{
				name: 'Lesson 1: Start Backend',
				description: 'Create new node js express project',
				link: 'https://www.youtube.com/embed/zb3Qk8SG5Ms',
			},
			{
				name: 'Lesson 2: Start Frontend',
				description: 'Create new react js project',
				link: 'https://www.youtube.com/embed/OIBIXYLJjsI',
			},
			{
				name: 'Lesson 3: Deployment',
				description: 'Deploy Mern Project on VPS',
				link: 'https://www.youtube.com/embed/-HPZ1leCV8k',
			},
		],
		public: true,
	},
	{
		_id: '3',
		name: 'C Sharp complete course',
		image: '/images/cshrp.jpg',
		description:
			'C# is a general-purpose, multi-paradigm programming language. C# encompasses static typing, strong typing, lexically scoped, imperative, declarative, functional, generic, object-oriented, and component-oriented programming disciplines.',
		rating: 3,
		numReviews: 12,
		authorId: 1,
		authorName: 'Tarek GuezGuez',
		authorImage: '/images/author.jpg',
		lessons: [
			{
				name: 'Lesson 1: Start Backend',
				description: 'Create new node js express project',
				link: 'https://www.youtube.com/embed/zb3Qk8SG5Ms',
			},
			{
				name: 'Lesson 2: Start Frontend',
				description: 'Create new react js project',
				link: 'https://www.youtube.com/embed/OIBIXYLJjsI',
			},
			{
				name: 'Lesson 3: Deployment',
				description: 'Deploy Mern Project on VPS',
				link: 'https://www.youtube.com/embed/-HPZ1leCV8k',
			},
		],
		public: true,
	},
	{
		_id: '4',
		name: 'Node js complete course',
		image: '/images/node.jpg',
		description:
			'Node.js is an open-source, cross-platform, back-end JavaScript runtime environment that runs on the V8 engine and executes JavaScript code outside a web browser.',
		rating: 5,
		numReviews: 12,
		authorId: 1,
		authorName: 'Tarek GuezGuez',
		authorImage: '/images/author.jpg',
		lessons: [
			{
				name: 'Lesson 1: Start Backend',
				description: 'Create new node js express project',
				link: 'https://www.youtube.com/embed/zb3Qk8SG5Ms',
			},
			{
				name: 'Lesson 2: Start Frontend',
				description: 'Create new react js project',
				link: 'https://www.youtube.com/embed/OIBIXYLJjsI',
			},
			{
				name: 'Lesson 3: Deployment',
				description: 'Deploy Mern Project on VPS',
				link: 'https://www.youtube.com/embed/-HPZ1leCV8k',
			},
		],
		public: true,
	},
	{
		_id: '5',
		name: 'React js complete course',
		image: '/images/react.jpg',
		description:
			'React is a free and open-source front-end JavaScript library for building user interfaces based on UI components. It is maintained by Meta and a community of individual developers and companies.',
		rating: 3.5,
		numReviews: 10,
		authorId: 1,
		authorName: 'Tarek GuezGuez',
		authorImage: '/images/author.jpg',
		lessons: [
			{
				name: 'Lesson 1: Start Backend',
				description: 'Create new node js express project',
				link: 'https://www.youtube.com/embed/zb3Qk8SG5Ms',
			},
			{
				name: 'Lesson 2: Start Frontend',
				description: 'Create new react js project',
				link: 'https://www.youtube.com/embed/OIBIXYLJjsI',
			},
			{
				name: 'Lesson 3: Deployment',
				description: 'Deploy Mern Project on VPS',
				link: 'https://www.youtube.com/embed/-HPZ1leCV8k',
			},
		],
		public: true,
	},
	{
		_id: '6',
		name: 'Mern Stack complete course',
		image: '/images/mern.jpg',
		description:
			'MEAN is a free and open-source JavaScript software stack for building dynamic web sites and web applications. Because all components of the MEAN stack support programs that are written in JavaScript, MEAN applications can be written in one language for both server-side and client-side execution environments.',
		rating: 4,
		numReviews: 12,
		authorId: 1,
		authorName: 'Tarek GuezGuez',
		authorImage: '/images/author.jpg',
		lessons: [
			{
				name: 'Lesson 1: Start Backend',
				description: 'Create new node js express project',
				link: 'https://www.youtube.com/embed/zb3Qk8SG5Ms',
			},
			{
				name: 'Lesson 2: Start Frontend',
				description: 'Create new react js project',
				link: 'https://www.youtube.com/embed/OIBIXYLJjsI',
			},
			{
				name: 'Lesson 3: Deployment',
				description: 'Deploy Mern Project on VPS',
				link: 'https://www.youtube.com/embed/-HPZ1leCV8k',
			},
		],
		public: true,
	},
	{
		_id: '7',
		name: 'private',
		image: '/images/mern.jpg',
		description: 'private',
		rating: 0,
		numReviews: 0,
		authorId: 1,
		authorName: 'Tarek GuezGuez',
		authorImage: '/images/author.jpg',
		lessons: [],
		public: false,
	},
]

module.exports = products
