const express = require('express')
const router = express.Router()

const {
	login,
	register,
	getItems,
	getItemById,
	updateItem,
	deleteItem,
	getItemProducts,
} = require('../controllers/user.controller')

router.route('/login').post(login)
router.route('/register').post(register)
router.route('/users').get(getItems)
router.route('/users/:id').get(getItemById)
router.route('/users/:id').put(updateItem)
router.route('/users/:id').delete(deleteItem)

router.route('/usersProducts/:id').get(getItemProducts)

module.exports = router
