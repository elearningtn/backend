const express = require('express')
const router = express.Router()

const {
	getItems,
	getItemById,
	createItem,
	updateItem,
	deleteItem,
	upload,
	uploadFile,
	generateSecret,
} = require('../controllers/products.controllers')

router.route('/products').get(getItems)
router.route('/products/:id').get(getItemById)
router.route('/products').post(createItem)
router.route('/products/:id').put(updateItem)
router.route('/products/:id').delete(deleteItem)
router.route('/products/upload').post(upload)
router.route('/products/uploadfile').post(uploadFile)
router.route('/products/secret/:id').put(generateSecret)

module.exports = router
