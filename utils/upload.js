const multer = require('multer')
const path = require('path')

const storage = multer.diskStorage({
	destination: 'public/images/',
	filename: function (req, file, cb) {
		const uniqueSuffix =
			Date.now() + '-' + Math.round(Math.random() * 1e9) + path.extname(file.originalname)
		cb(null, path.parse(file.originalname).name + '-' + uniqueSuffix)
	},
})

const upload = multer({ storage: storage })

module.exports = upload
