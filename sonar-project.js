const sonarqubeScanner =  require('sonarqube-scanner');
sonarqubeScanner(
    {
        serverUrl:  'http://51.178.220.115:9000',
        options : {
						'sonar.sources':  '.',
						'sonar.sourceEncoding': 'UTF-8',
						'sonar.login': 'admin',
						'sonar.password': 'a11a22a22',
            'sonar.inclusions'  :  '**', // Entry point of your code
        }
    }, () => {});
