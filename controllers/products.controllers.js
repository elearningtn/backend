const Product = require('../models/product.model')
const AsyncHandler = require('express-async-handler')
const upload = require('../utils/upload')
const uploadImage = upload.single('image')
const path = require('path')
const fs = require('fs')
const uploadFile = require('../utils/uploadFile')
const uploadPdf = uploadFile.single('file')

exports.getItems = AsyncHandler(async (req, res, next) => {
        const items = await Product.find()

        return res.status(200).json({
                success: true,
                items,
        })
})


exports.getItemById = AsyncHandler(async (req, res, next) => {
        const item = await Product.findById(req.params.id)

        if (!item) {
                return res.status(404).json({
                        success: false,
                        message: 'No Result Found!',
                })
        }

        return res.status(200).json({
                success: true,
                item,
        })
})

exports.createItem = AsyncHandler(async (req, res, next) => {
        try {
                req.body.authorName = req.body.user.name
                req.body.authorImage = req.body.user.image

                const item = await Product.create(req.body)
                return res.status(201).json({
                        success: true,
                        item,
                })
        } catch (error) {
                return res.status(500).send(error)
        }
})

exports.updateItem = AsyncHandler(async (req, res, next) => {
        let item = await Product.findById(req.params.id)

        if (!item) {
                return res.status(404).json({
                        success: false,
                        message: 'No Result Found!',
                })
        }

        try {
                item = await Product.findByIdAndUpdate(req.params.id, req.body, {
                        new: true,
                        runValidators: true,
                        useFindAndModify: false,
                })

                return res.status(200).json({
                        success: true,
                        item,
                })
        } catch (error) {
                return res.status(500).send(error)
        }
})

exports.deleteItem = AsyncHandler(async (req, res, next) => {
        const item = await Product.findById(req.params.id)

        if (!item) {
                return res.status(404).json({
                        success: false,
                        message: 'No Result Found!',
                })
        }

        const link = path.join(
                __dirname,
                '../public',
                item.image.split('http://51.178.220.115:4200')[1].toString()
        )

        await fs.unlink(link, async (err) => {
                await item.remove()
        })

        return res.status(200).json({
                success: true,
                message: 'Item deleted successfully.',
        })
})

exports.upload = AsyncHandler(async (req, res, next) => {
        uploadImage(req, res, function (err) {
                if (err) {
                        return res.status(500).json({
                                success: false,
                                message: err.message,
                        })
                } else {
                        return res.status(201).json({
                                success: true,
                                image: 'http://51.178.220.115:4200/images/' + res.req.file.filename,
                        })
                }
        })
})

exports.uploadFile = AsyncHandler(async (req, res, next) => {
        uploadPdf(req, res, function (err) {
                if (err) {
                        return res.status(500).json({
                                success: false,
                                message: err.message,
                        })
                } else {
                        return res.status(201).json({
                                success: true,
                                image: 'http://51.178.220.115:4200/files/' + res.req.file.filename,
                        })
                }
        })
})

exports.generateSecret = AsyncHandler(async (req, res, next) => {
        let item = await Product.findById(req.params.id)

        if (!item) {
                return res.status(404).json({
                        success: false,
                        message: 'No Result Found!',
                })
        }

        try {
                const productSecret = req.body._id + '_secret_' + Math.random().toString(36).substr(2, 8)

                item = await Product.findByIdAndUpdate(
                        req.params.id,
                        { productSecret },
                        {
                                new: true,
                                runValidators: true,
                                useFindAndModify: false,
                        }
                )

                return res.status(200).json({
                        success: true,
                        item,
                })
        } catch (error) {
                return res.status(500).send(error)
        }
})
