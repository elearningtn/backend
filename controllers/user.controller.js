const User = require('../models/user.model')
const Product = require('../models/product.model')
const AsyncHandler = require('express-async-handler')
const generateToken = require('../utils/generateToken.js')

exports.login = AsyncHandler(async (req, res, next) => {
        const { email, password } = req.body

        const user = await User.findOne({ email })

        if (user && (await user.matchPassword(password))) {
                return res.json({
                        _id: user._id,
                        name: user.name,
                        email: user.email,
                        isAdmin: user.isAdmin,
                        isSuperAdmin: user.isSuperAdmin,
                        token: generateToken(user._id),
                })
        } else {
                return res.status(401).json({
                        success: false,
                        message: 'Invalid email or password',
                })
        }
})

exports.register = AsyncHandler(async (req, res, next) => {
        const { name, email, password } = req.body

        const userExists = await User.findOne({ email })

        if (userExists) {
                return res.status(400).json({
                        success: false,
                        message: 'User already exists',
                })
        }

        const user = await User.create({
                name,
                email,
                password,
        })

        if (user) {
                return res.status(201).json({
                        _id: user._id,
                        name: user.name,
                        email: user.email,
                        isAdmin: user.isAdmin,
                        isSuperAdmin: user.isSuperAdmin,
                        token: generateToken(user._id),
                })
        } else {
                return res.status(400).json({
                        success: false,
                        message: 'Invalid user data',
                })
        }
})

exports.getItems = AsyncHandler(async (req, res, next) => {
        const items = await User.find({})

        return res.status(200).json({
                success: true,
                items,
        })
})

exports.getItemById = AsyncHandler(async (req, res, next) => {
        const item = await User.findById(req.params.id)

        if (!item) {
                return res.status(404).json({
                        success: false,
                        message: 'No Result Found!',
                })
        }

        return res.status(200).json({
                success: true,
                item,
        })
})

exports.updateItem = AsyncHandler(async (req, res, next) => {
        let item = await User.findById(req.params.id)

        if (!item) {
                return res.status(404).json({
                        success: false,
                        message: 'No Result Found!',
                })
        }

        try {
                item = await User.findByIdAndUpdate(req.params.id, req.body, {
                        new: true,
                        runValidators: true,
                        useFindAndModify: false,
                })

                return res.status(200).json({
                        success: true,
                        item,
                })
        } catch (error) {
                return res.status(500).send(error)
        }
})

exports.deleteItem = AsyncHandler(async (req, res, next) => {
        const item = await User.findById(req.params.id)

        if (!item) {
                return res.status(404).json({
                        success: false,
                        message: 'No Result Found!',
                })
        }

        await item.remove()

        return res.status(200).json({
                success: true,
                message: 'Item deleted successfully.',
        })
})

exports.getItemProducts = AsyncHandler(async (req, res, next) => {
        const item = await User.findById(req.params.id)

        if (!item) {
                return res.status(404).json({
                        success: false,
                        message: 'No Result Found!',
                })
        }

        const items = await Product.find({
                user: item,
        })

        return res.status(200).json({
                success: true,
                user: item,
                products: items,
        })
})
