FROM node:16.13

# Create app directory
WORKDIR /usr/src/app
COPY package*.json ./
RUN npm install
# Make uploads directory setup path.
ENV TZ=Africa/Tunis
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
RUN export NODE_ENV=production
# Bundle app sources

COPY . .
EXPOSE 4200
RUN node  sonar-project.js
CMD [ "node", "server.js" ]

